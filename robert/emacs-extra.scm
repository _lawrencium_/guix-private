(define-module (robert emacs-extra)
 #:use-module (guix packages)
 #:use-module (guix download)
 #:use-module (guix build-system emacs)
 #:use-module ((guix licenses) #:prefix license:)
 #:use-module (gnu packages)
 #:use-module (gnu packages emacs-xyz))

(define-public emacs-persist
(package
  (name "emacs-persist")
  (version "0.4")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://elpa.gnu.org/packages/persist-"
             version
             ".tar"))
      (sha256
        (base32
          "0gpxy41qawzss2526j9a7lys60vqma1lvamn4bfabwza7gfhac0q"))))
  (build-system emacs-build-system)
  (home-page
    "http://elpa.gnu.org/packages/persist.html")
  (synopsis
    "Persist Variables between Emacs Sessions")
  (description
    "This package provides variables which persist across sessions.

The main entry point is `persist-defvar' which behaves like
`defvar' but which persists the variables between session.  Variables
are automatically saved when Emacs exits.

Other useful functions are `persist-save' which saves the variable
immediately, `persist-load' which loads the saved value,
`persist-reset' which resets to the default value.

Values are stored in a directory in `user-emacs-directory', using
one file per value.  This makes it easy to delete or remove unused
variables.")
  (license license:gpl3+)))

(define-public emacs-org-drill
(package
  (name "emacs-org-drill")
  (version "20191219.2100")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://melpa.org/packages/org-drill-"
             version
             ".el"))
      (sha256
        (base32
          "0nbsn1vi2g68yvy74r3bmywfkgpm4a5nmx5g9sxy5fb185lrkzc8"))))
  (build-system emacs-build-system)
  (propagated-inputs
    `(("emacs-seq" ,emacs-seq)
      ("emacs-org" ,emacs-org)
      ("emacs-persist" ,emacs-persist)))
  (home-page
    "https://gitlab.com/phillord/org-drill/issues")
  (synopsis "Self-testing using spaced repetition")
  (description
    "Within an Org mode outline or outlines, headings and associated content are
treated as \"flashcards\".  Spaced repetition algorithms are used to conduct
interactive \"drill sessions\", where a selection of these flashcards is
presented to the student in random order.  The student rates his or her
recall of each item, and this information is used to schedule the item for
later revision.

Each drill session can be restricted to topics in the current buffer
(default), one or several files, all agenda files, or a subtree.  A single
topic can also be tested.

Different \"card types\" can be defined, which present their information to
the student in different ways.

See the file README.org for more detailed documentation.
")
  (license #f)))

