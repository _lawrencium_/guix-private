



(define-module (robert tweaks)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils)
  #:use-module (gnu packages vim)


  #:use-module (gnu packages bison)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages nss)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages re2c)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (srfi srfi-1))

;;(define-public emacs-elfeed-org
;;  (package
;;    (name "emacs-elfeed-org")
;;    (version "20181015.1100")
;;    (source
;;      (origin
;;        (method url-fetch)
;;        (uri (string-append
;;             "https://melpa.org/packages/elfeed-org-"
;;             version
;;             ".el"))
;;        (sha256
;;          (base32
;;            "1bawfxgijlfa4wska2znl69wfdzh53gh9arf180l71fhjlvablaw"))))
;;    (build-system emacs-build-system)
;;    (propagated-inputs
;;      `(("emacs-elfeed" ,emacs-elfeed)
;;        ("emacs-org" ,emacs-org)
;;        ("emacs-dash" ,emacs-dash)
;;        ("emacs-s" ,emacs-s)))
;;    (home-page
;;      "https://github.com/remyhonig/elfeed-org")
;;    (synopsis
;;      "Configure elfeed with one or more org-mode files")
;;    (description
;;      "Maintaining tags for all rss feeds is cumbersome using the regular system")
;;    (license #f))) ;; just for testing, not real license

(define-public vim-with-x
(package  (inherit vim)
	(name "vim-with-x")
	(arguments (append 
				 `(#:configure-flags (list "--with-x")) 
				 (package-arguments vim)))
	(inputs (append
			  (package-inputs vim)
			  `(("libx11" ,libx11)
			    ("libxt" ,libxt)
				("libsm" ,libsm)
				("libxpm" ,libxpm) ;Doesn't seem to be needed
				("libxtst" ,libxtst))))))

;(define-public qtwebengine
;  (package
;    (name "qtwebengine")
;    (version (package-version qtbase))
;    (source
;     (origin
;       (method url-fetch)
;       (uri
;        (string-append "https://download.qt.io/official_releases/qt/"
;                       (substring version 0 4)
;                       "/" version "/submodules/"
;                       (string-append name "-everywhere-src-" version)
;                       ".tar.xz"))
;       (sha256
;        (base32
;         "1zmqsdais85cdfh2jh8h4a5jcamp1mzdk3vgqm6xnldqf6nrxd2v"))))
;    (build-system gnu-build-system)
;    (native-inputs
;     `(
;       ("perl" ,perl)
;       ("python-2" ,python-2)
;       ("pkg-config" ,pkg-config)
;       ("flex" ,flex)
;       ("bison" ,bison)
;       ("ruby" ,ruby)
;       ("ninja" ,ninja)
;       ))
;    (inputs
;     `(
;       ("qtbase" ,qtbase)
;       ("qtdeclarative" ,qtdeclarative)
;       ("libxcb" ,libxcb)
;       ("xcb-util" ,xcb-util)
;       ("libxkbcommon" ,libxkbcommon)
;       ("libx11" ,libx11)
;       ("libxrender" ,libxrender)
;       ("libxi" ,libxi)
;       ;; OpenGL
;       ("mesa" ,mesa)
;       ;; qt web engine
;       ("libgcrypt" ,libgcrypt)
;       ("pciutils" ,pciutils)
;       ("nss" ,nss)
;       ("libxtst" ,libxtst)
;       ("gperf" ,gperf)
;       ("cups-minimal" ,cups-minimal)
;       ("pulseaudio" ,pulseaudio)
;       ("udev" ,eudev)
;       ;; systemd-devel? no systemd on guix
;       ("libcap" ,libcap)
;       ("alsa-lib" ,alsa-lib)
;       ("dbus" ,dbus)
;       ("libxrandr" ,libxrandr)
;       ("libxcomposite" ,libxcomposite)
;       ("libxcursor" ,libxcursor)
;       ("fontconfig" ,fontconfig)
;       ("qtwebchannel" ,qtwebchannel)
;       ("atk" ,atk)
;       ("qtmultimedia" ,qtmultimedia)
;       ("re2c" ,re2c)
;       ))
;    (arguments
;     `(#:phases
;       (modify-phases %standard-phases
;         (add-before 'configure 'configure-qmake
;           (lambda* (#:key inputs outputs #:allow-other-keys)
;             (let* ((out (assoc-ref outputs "out"))
;                    (qtbase (assoc-ref inputs "qtbase"))
;                    (tmpdir (string-append (getenv "TMPDIR")))
;                    (qmake (string-append tmpdir "/qmake"))
;                    (qt.conf (string-append tmpdir "/qt.conf")))
;               ;; Use qmake with a customized qt.conf to override install
;               ;; paths to $out.
;               (symlink (which "qmake") qmake)
;               (setenv "CC" "gcc")
;               (setenv "PATH" (string-append tmpdir ":" (getenv "PATH")))
;               (with-output-to-file qt.conf
;                 (lambda ()
;                   (format #t "[Paths]
;Prefix=~a
;ArchData=lib/qt5
;Data=share/qt5
;Documentation=share/doc/qt5
;Headers=include/qt5
;Libraries=lib
;LibraryExecutables=lib/qt5/libexec
;Binaries=bin
;Tests=tests
;Plugins=lib/qt5/plugins
;Imports=lib/qt5/imports
;Qml2Imports=lib/qt5/qml
;Translations=share/qt5/translations
;Settings=etc/xdg
;Examples=share/doc/qt5/examples
;HostPrefix=~a
;HostData=lib/qt5
;HostBinaries=bin
;HostLibraries=lib
;
;[EffectiveSourcePaths]
;HostPrefix=~a
;HostData=lib/qt5
;" out out qtbase)))
;               #t)))
;         (replace 'configure
;           (lambda* (#:key inputs outputs #:allow-other-keys)
;             ;; Valid QT_BUILD_PARTS variables are:
;             ;; libs tools tests examples demos docs translations
;             (invoke "qmake" "QT_BUILD_PARTS = libs tools")))
;         (add-before 'check 'set-display
;           (lambda _
;             ;; make Qt render "offscreen", required for tests
;             (setenv "QT_QPA_PLATFORM" "offscreen")
;             #t))
;         (add-after 'install-binaries 'install-qt.conf
;           (lambda* (#:key inputs outputs #:allow-other-keys)
;             (let* ((out (assoc-ref outputs "out"))
;                    (tmpdir (string-append (getenv "TMPDIR")))
;                    (in.conf (string-append tmpdir "/qt.conf"))
;                    (out.conf (string-append out "/lib/qt5/libexec/qt.conf")))
;               (copy-file in.conf out.conf))
;             #t))
;         )))
;    (home-page "https://www.qt.io")
;    (synopsis "Qt5WebEngine")
;    (description "Qt5WebEngine provides support for web
;applications using the Chromium browser project.")
;    (license
;     (package-license qt))))

